from datetime import datetime
import json
import logging
import os
import sys
import traceback
sys.path.insert(0, 'packages')
import boto3
import feedparser
import requests

# Lambda entry point; posts RSS feed updates to Discord via Webhook.
def lambda_handler(event, lambda_context):

    logging.getLogger().setLevel(os.environ["LOGGER_LEVEL"])
    bucket_name = os.environ['BUCKET_NAME']
    notification_topic_arn = os.environ['NOTIFY_TOPIC_ARN']

    sources_updated = False
    updated_sources = []

    # Get the sources from S3
    sources_json = json.loads(boto3.resource('s3').Object(
        bucket_name,
        'lambda/kokoro/sources.json').get()['Body'].read().decode('utf-8'))
    logging.info(f'Sources retrieved: {len(sources_json)}.')

    # Check each enabled source's RSS feed for updates
    for source in sources_json:
        if source['enabled']:
            try:
                rss_feed = feedparser.parse(source['rss_feed_url'])
                if not rss_feed.entries or len(rss_feed.entries) == 0:
                    # No results in feed; continue to next loop
                    logging.warning(f'Entry: {source["name"]} could not be checked for updates; no entries found in feed.')
                    continue

                latest_rss_feed_entry = rss_feed.entries[0]

                if latest_rss_feed_entry.title != source['latest_title']:

                    # Update the source title
                    source['latest_title'] = latest_rss_feed_entry.title
                    updated_sources.append(source['name'])
                    if not sources_updated:
                        sources_updated = True

                    # Get a list of users to tag in the embed
                    user_mentions = ''
                    for user_id in os.environ['TAGGED_USER_IDS'].split(','):
                        user_mentions += f'<@{user_id}> '

                    # Generate embed data to post to webhook
                    discord_data = {
                        'username': os.environ['DISPLAY_USERNAME'],
                        'avatar_url': os.environ['DISPLAY_AVATAR_URL'],
                        'embeds': [
                            {
                                'title': f'A new chapter has been released: {source["latest_title"]}',
                                'description': f'You are receiving this message because a new chapter for {source["name"]} was released!',
                                'color': 12370112,
                                'url': latest_rss_feed_entry.link,
                                'fields': [
                                    {
                                        'name': 'Tags',
                                        'value': user_mentions,
                                        'inline': False
                                    }
                                ],
                                'image': {
                                    'url': rss_feed.feed.image.href
                                },
                                'footer': {
                                    'text': f'Delivered: {datetime.now().strftime(r"%d/%m/%Y, %H:%M")}'
                                }
                            }
                        ]
                    }

                    # Post to webhook
                    headers = {'content-type': 'application/json'}
                    response = requests.post(
                        url=os.environ['WEBHOOK_URL'],
                        data=json.dumps(discord_data),
                        headers=headers)
                    logging.info(f'Webhook request response was: {response.status_code}.')

            except:
                # Notify alert topic on failure, move on to next source
                logging.error('Failed to post to webhook; an unexpected error occurred.')
                boto3.client('sns').publish(TopicArn=notification_topic_arn,
                    Subject='kokoro failed: an unexpected error occurred.',
                    Message=traceback.format_exc(),
                    MessageAttributes={
                        'DiscordDisplayAllData': {
                            'DataType': 'String',
                            'StringValue': 'true'
                        }
                    })
                continue
        else:
            logging.info(f'Source at: {source["rss_feed_url"]} is disabled, skipping.')

    # Update the sources file on S3 if any source was updated
    if sources_updated:
        try:
            boto3.resource('s3').Object(
                bucket_name,
                'lambda/kokoro/sources.json').put(Body=bytes(json.dumps(sources_json).encode('UTF-8')))
            logging.info('Updated sources.json')
            logging.info(f'The following sources were updated: {", ".join(updated_sources)}')

        except:
            # Notify alert topic on failure and raise the exception
            logging.error('Failed to updates sources.json; an unexpected error occurred.')
            boto3.client('sns').publish(TopicArn=notification_topic_arn,
                Subject='kokoro failed: sources.json could not be updated.',
                Message=traceback.format_exc(),
                MessageAttributes={
                    'DiscordDisplayAllData': {
                        'DataType': 'String',
                        'StringValue': 'true'
                    },
                    'SourcesJson': {
                        'DataType': 'String',
                        'StringValue': json.dumps(sources_json)
                    }
                })
            raise

    return 'Done.'

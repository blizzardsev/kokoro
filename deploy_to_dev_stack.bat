@echo off
Rem This script will automatically deploy the DEV stack for the kokoro solution.
Rem Params:
Rem %1: Version (string) : The major, minor and subminor version of this deployment.
Rem %2: Webhook (string) : The webhook URL for this deployment.
Rem Execute usage: deploy_to_dev_stack.bat 0.0.1
Rem Refer to GitLab env vars on CI config for current version.

echo Fetching requirements...
rem Builds requirements.txt, whilst ignoring any packages already present. CI deployments rely on this!
cd scripts
pigar -i packages
cd ..

echo Downloading dependencies...
rem Installs dependencies to the packages directory. This is then referenced in the main function file.
pip install -r scripts/requirements.txt -t scripts/packages --upgrade

echo Deploying..
cmd /C call deploy/deploy.bat %1 %2
echo Deployment complete.

echo Script completed. Last updated: %time%
pause
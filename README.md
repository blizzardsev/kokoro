# About kokoro

Kokoro is a simple Python-based serverless function for parsing manga RSS feeds and delivering notifications of new chapters to a specified Discord channel via webhook.

## CI/deployment script configuration

### Dependencies

* **AWS CLI**: Command-line toolkit for AWS @ https://aws.amazon.com/cli/
* **AWS SAM CLI**: Command-line toolkit for AWS SAM @ https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html
* **pigar**: Automatic Python requirements.txt @ https://github.com/damnever/pigar

## Python configuration

### Runtime

* Python 3.8

### Dependencies

* Boto3: AWS toolkit @ https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html
* FeedParser: RSS parser @ https://feedparser.readthedocs.io/en/latest/#
* Requests: HTTP for Humans @ https://requests.readthedocs.io/en/master/

### Environment variables

* **ALERT_TOPIC_ARN** : The ARN of the alert topic to send notifications to in the event of controlled failure.
* **BUCKET_NAME** : The name of the bucket (**not** ARN/URL) containing the sources.json file, under key `lambda/kokoro/sources.json`.
* **DISPLAY_AVATAR_URL** : The URL specifying the profile picture to display for the sender of the messages on Discord.
* **DISPLAY_USERNAME** : The username to display as the sender of the messages on Discord.
* **LOGGER_LEVEL** : The minimum log level to output on CloudWatch logs.
* **TAGGED_USER_IDS** : The IDs of the users to tag on Discord. Each ID must be comma-separated.
* **WEBHOOK_URL** : The URL identifying the channel on Discord to post to.

## Deployment

Use script *deploy_to_dev_stack.bat* *version-number* for manual deployments; E.G `deploy_to_dev_stack.bat 1.0.0`. Execute within Kokoro directory.
Release deployments are handled by CI.

## Contact

*blizzardsev.dev@gmail.com*